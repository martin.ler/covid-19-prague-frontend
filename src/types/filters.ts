import { SamplingPointsServiceRequestTypeEnum } from 'api';

export enum DistrictFilter {
  'PHA',
  'STC',
  'ANY',
}

export enum MethodFilter {
  'WALK_WITHOUT_RESERVATION',
  'IN_CAR',
  'WALK_WITH_RESERVATION',
}

export enum PriceFilter {
  'LOW',
  'HIGH',
  'ANY',
}

export type changeFilterFn = <T extends keyof Filters>(name: T, value: Filters[T]) => void;

export type Filters = {
  districtFilter: DistrictFilter;
  paymentFilter: SamplingPointsServiceRequestTypeEnum;
  methodFilter: MethodFilter;
  priceFilter: PriceFilter;
};
