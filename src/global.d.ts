export {};

declare global {
  interface Window {
    config: {
      API_URL: string;
      MAPBOX_TOKEN: string;
      EMAIL: string;
      GA_CODE: string;
      DATA_REFRESH_WARNING_DELAY: number;
    };
  }
}
