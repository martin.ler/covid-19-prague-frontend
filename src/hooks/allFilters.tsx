import { SamplingPointsService } from 'api';
import { useMethodFilter } from 'hooks/methodFilter';
import { usePaymentFilter } from 'hooks/paymentFilter';
import { useDistrictFilter } from 'hooks/districtFilter';
import { usePriceFilter } from 'hooks/priceFilter';
import { SamplingPointWithGeometry } from 'types/data';
import { Filters } from 'types/filters';

export enum FilterResultEnum {
  ALL_PASSED,
  MAIN_PASSED,
  FAILED,
}

export const useFilters = () => {
  const { hasAppropriatePrice } = usePriceFilter();
  const { hasAppropriateMethod } = useMethodFilter();
  const { hasAppropriatePayment } = usePaymentFilter();
  const { hasAppropriateDistrict } = useDistrictFilter();

  return {
    getFilterResult: (
      service: SamplingPointsService,
      point: SamplingPointWithGeometry | undefined,
      filters: Filters
    ) => {
      if (
        !hasAppropriateMethod(service, filters) ||
        !hasAppropriatePayment(service, filters) ||
        !hasAppropriateDistrict(point?.district, filters)
      ) {
        return FilterResultEnum.FAILED;
      } else if (!hasAppropriatePrice(service, filters)) {
        return FilterResultEnum.MAIN_PASSED;
      } else {
        return FilterResultEnum.ALL_PASSED;
      }
    },
  };
};
