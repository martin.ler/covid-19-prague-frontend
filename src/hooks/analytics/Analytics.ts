import { SortColumn } from 'types/sorting';
import ga from 'react-ga';
import { Filters, DistrictFilter, MethodFilter, PriceFilter } from 'types/filters';

interface ServiceParams {
  serviceId: number;
  pointId: number;
}

/**
 * Singleton Analytics with implementation of React Google Analytics
 */
export class Analytics {
  private static instance: Analytics;

  private constructor() {
    ga.initialize(window.config.GA_CODE);
  }

  public static getInstance(): Analytics {
    if (!Analytics.instance) {
      Analytics.instance = new Analytics();
    }

    return Analytics.instance;
  }

  public openPage(url: string) {
    ga.pageview(url);
  }

  public openModal({ serviceId, pointId }: ServiceParams) {
    ga.modalview(`/${serviceId}/${pointId}`);
    ga.event({
      category: 'User',
      action: 'Open Modal',
      label: pointId.toString(),
      value: serviceId,
    });
  }

  public openAbout(type: string) {
    ga.event({
      category: 'User',
      action: 'Open About Modal',
      label: type,
    });
  }

  public changeFilter(filters: Filters, isDefault: boolean = false) {
    let label = '';
    label += isDefault ? '[default] ' : '';
    label += DistrictFilter[filters.districtFilter] + ',';
    label += MethodFilter[filters.districtFilter] + ',';
    label += 'PRICE_' + PriceFilter[filters.priceFilter];
    ga.event({
      category: 'User',
      action: 'Change filter',
      label: label,
    });
  }

  public changeDataView(shouldShowMap: boolean, isDefault: boolean = false) {
    ga.event({
      category: 'User',
      action: 'Change data view',
      label: (isDefault ? '[default] ' : '') + (shouldShowMap ? 'map' : 'table'),
    });
  }

  public changeSort(sortBy: SortColumn, isDefault: boolean = false) {
    ga.event({
      category: 'User',
      action: 'Change sorting',
      label: (isDefault ? '[default] ' : '') + sortBy,
    });
  }

  public changeService({ serviceId, pointId }: ServiceParams, isDefault: boolean = false) {
    ga.event({
      category: 'User',
      action: 'Change Service',
      label: (isDefault ? '[default] ' : '') + pointId.toString(),
      value: serviceId,
    });
  }

  public book({ serviceId, pointId, url }: ServiceParams & { url: string }) {
    ga.outboundLink({ label: url }, () => {
      ga.event({
        category: 'External Navigation',
        action: 'Click on the Link to book a service',
        label: pointId.toString(),
        value: serviceId,
      });
    });
  }

  public openExternalLink(url: string) {
    ga.outboundLink({ label: url }, () => {});
  }
}
