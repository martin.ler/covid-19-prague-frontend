import { SamplingPointsServiceRequestTypeEnum } from 'api';
import { DistrictFilter, Filters, MethodFilter, PriceFilter } from 'types/filters';
import { SortColumn, TableColumns } from 'types/sorting';
import { prices } from 'hooks/priceFilter';
import { useTranslation } from 'react-i18next';
import { useMemo } from 'react';

export const useSubHeading = (
  filters: Filters,
  columns: TableColumns,
  sortByColumn?: SortColumn
) => {
  const { t } = useTranslation();

  const subHeading = useMemo(() => {
    const districtString =
      (filters.districtFilter === DistrictFilter.PHA && t('filters.prague')) ||
      (filters.districtFilter === DistrictFilter.STC && t('filters.centralBohemia')) ||
      (filters.districtFilter === DistrictFilter.ANY && t('filters.pragueAndCentralBohemia'));
    const paymentString =
      filters.paymentFilter === SamplingPointsServiceRequestTypeEnum.Application
        ? t('filters.withRequest').toLowerCase()
        : t('filters.selfpaid').toLowerCase();

    const methodString =
      (filters.methodFilter === MethodFilter.WALK_WITHOUT_RESERVATION &&
        t('filters.walkWithoutReservation').toLowerCase()) ||
      (filters.methodFilter === MethodFilter.WALK_WITH_RESERVATION &&
        t('filters.walkWithReservation').toLowerCase()) ||
      (filters.methodFilter === MethodFilter.IN_CAR && t('filters.inCar').toLowerCase());

    const getPriceFilterName = (priceFilter: PriceFilter) => {
      switch (priceFilter) {
        case PriceFilter.ANY:
          return t('filters.any').toLowerCase();
        case PriceFilter.HIGH:
          return t('filters.priceOrLess', {
            price: prices.high,
          }).toLowerCase();
        case PriceFilter.LOW:
          return t('filters.priceOrLess', { price: prices.low }).toLowerCase();
      }
    };

    const priceString = columns.showPriceColumn
      ? `${t('filters.price').toLowerCase()} ${getPriceFilterName(filters.priceFilter)}`
      : null;

    const filterString =
      (sortByColumn === SortColumn.PRICE && t('databoxSubheadingOrder.byPrice')) ||
      (sortByColumn === SortColumn.WAIT_TIME_OR_CLOSEST_SLOT &&
        columns.showClosestSlotColumn &&
        t('databoxSubheadingOrder.byTime')) ||
      (sortByColumn === SortColumn.WAIT_TIME_OR_CLOSEST_SLOT &&
        columns.showExpectedWaitTime &&
        t('databoxSubheadingOrder.byWaitTime')) ||
      t('databoxSubheadingOrder.byResultTime');

    return [
      districtString,
      paymentString,
      methodString,
      ...(priceString ? [priceString] : []),
      ...(sortByColumn ? [filterString] : []),
    ].join(', ');
  }, [filters, columns, sortByColumn, t]);

  return { subHeading };
};
