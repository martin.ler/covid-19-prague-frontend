import { SamplingPointsService } from 'api';
import { Filters } from 'types/filters';

export const usePaymentFilter = () => {
  return {
    hasAppropriatePayment: (service: SamplingPointsService, filters: Filters): boolean => {
      return filters.paymentFilter === service.requestType;
    },
  };
};
