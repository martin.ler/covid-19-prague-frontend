import { SamplingPointDistrictEnum } from 'api';
import { Filters, DistrictFilter } from 'types/filters';

export const useDistrictFilter = () => {
  return {
    hasAppropriateDistrict: (
      district: SamplingPointDistrictEnum | undefined,
      filters: Pick<Filters, 'districtFilter'> & Partial<Filters>
    ): boolean => {
      if (!district) return true;

      switch (filters.districtFilter) {
        case DistrictFilter.PHA:
          return district === SamplingPointDistrictEnum.PHA;
        case DistrictFilter.STC:
          return district === SamplingPointDistrictEnum.STC;
        case DistrictFilter.ANY:
          return true;
      }
    },
  };
};
