import React, { createContext, FC, useCallback, useEffect, useMemo, useState } from 'react';
import {
  Configuration,
  SamplingPointsApi,
  SamplingPointsLangEnum,
  SamplingPointsOutputFeatures,
} from 'api';
import { SamplingPointWithGeometry, Service, WeightedService } from 'types/data';
import { getWaitTimeState } from 'utils/waitTime';
import { Filters } from 'types/filters';
import { useFilters } from 'hooks/allFilters';
import { getSamplingPoint } from 'components/dataBox/utils';

const api = new SamplingPointsApi(new Configuration({ basePath: window.config.API_URL }));
const fetchData = async (lang: SamplingPointsLangEnum) => api.samplingPoints({ lang });

const getSamplingPoints = (
  features: SamplingPointsOutputFeatures[] | undefined
): SamplingPointWithGeometry[] => {
  if (!features) {
    return [];
  }
  return features.map((samplingPoint) => ({
    ...samplingPoint.properties,
    geometry: samplingPoint.geometry,
  }));
};

const getServices = (samplingPoints: SamplingPointWithGeometry[] | undefined): Service[] => {
  if (!samplingPoints) {
    return [];
  }
  return samplingPoints.reduce((acc: Service[], { services }) => {
    if (!services) return acc;

    const pointServices = services.map((service) => ({
      ...service,
      waitTimeState: getWaitTimeState({
        queue: service.queue,
        openingHours: service.openingHours,
      }),
    }));

    return [...acc, ...pointServices];
  }, []);
};

export type DataState = {
  points: SamplingPointWithGeometry[];
  services: WeightedService[];
  refreshData: () => void;
  isLoading: boolean;
  hasError: boolean;
};

const defaultState = {
  points: [],
  services: [],
  refreshData: () => {},
  isLoading: false,
  hasError: false,
};

export const DataContext = createContext<DataState>(defaultState);

export const DataProvider: FC<{ filters: Filters; lang: SamplingPointsLangEnum }> = ({
  children,
  filters,
  lang,
}) => {
  const [isLoading, setIsLoading] = useState(defaultState.isLoading);
  const [hasError, setHasError] = useState(defaultState.hasError);
  const [points, setPoints] = useState<SamplingPointWithGeometry[]>(defaultState.points);

  const { getFilterResult } = useFilters();

  const refreshData = useCallback(async () => {
    try {
      setIsLoading(true);
      const data = await fetchData(lang);
      const newPoints = getSamplingPoints(data?.features);
      setPoints(newPoints);
      setHasError(false);
    } catch (error) {
      console.error(error);
      setHasError(true);
    } finally {
      setIsLoading(false);
    }
  }, [lang]);

  useEffect(() => {
    refreshData();
  }, [refreshData, lang]);

  const services = useMemo(() => getServices(points), [points]);

  const weightedServices = useMemo(() => {
    if (!services) {
      return services;
    }

    return services.map((service) => {
      const samplingPoint = getSamplingPoint(service.samplingPointsId, points);
      return {
        ...service,
        filterResult: getFilterResult(service, samplingPoint, filters),
      };
    });
  }, [filters, getFilterResult, points, services]);

  const state = useMemo((): DataState => {
    return { isLoading, hasError, points, services: weightedServices, refreshData };
  }, [isLoading, hasError, points, weightedServices, refreshData]);

  return <DataContext.Provider value={state}>{children}</DataContext.Provider>;
};
