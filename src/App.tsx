import { MainView } from 'components/mainView';
import { useAnalytics } from 'hooks/analytics';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { BrowserRouter, Redirect, Route } from 'react-router-dom';
import { LanguageProvider } from 'state/languageState';
import './i18n';

function App() {
  const { t } = useTranslation();
  const title = t('pageTitle');
  const [analytics] = useAnalytics();

  useEffect(() => {
    document.title = title;
  }, [title]);

  return (
    <LanguageProvider>
      <BrowserRouter basename={process.env.PUBLIC_URL}>
        <Route exact={true} strict={true} path="/">
          <>
            {analytics.openPage(window.location.pathname)}
            <MainView />
          </>
        </Route>
        <Route>
          <Redirect to="/" />
        </Route>
      </BrowserRouter>
    </LanguageProvider>
  );
}

export default App;
