import { SamplingPointsServicesOpeningHours, SamplingPointsServicesQueue } from 'api';
import moment from 'moment-timezone';
import { weekdays } from 'utils/weekdays';

export enum WaitTimeStateEnum {
  Short,
  Specified,
  Closed,
  Unknown,
}

export type GetWaitTimeStateArg = {
  queue?: SamplingPointsServicesQueue | null;
  openingHours?: SamplingPointsServicesOpeningHours[] | null;
};

export const getWaitTimeState = ({
  queue,
  openingHours,
}: GetWaitTimeStateArg): WaitTimeStateEnum => {
  const weekday = weekdays[moment().weekday()];

  const openingHoursToday = (openingHours || []).filter((dayHours) => {
    return dayHours.weekday === weekday;
  });

  const hoursToday = moment().format('HH:mm');

  const openingHoursNow = openingHoursToday.find((hours) => {
    return hours.startAt <= hoursToday && hours.endAt > hoursToday;
  });

  if (!openingHoursNow) {
    return WaitTimeStateEnum.Closed;
  }

  if (queue?.timestampCreated && moment().diff(moment(queue.timestampCreated), 'minutes') > 120) {
    // there is no / short queue if queue length has not been updated in over 2 hours
    return WaitTimeStateEnum.Short;
  }

  if (!queue || queue.waitMin === null || queue.waitMin === undefined) {
    return WaitTimeStateEnum.Unknown;
  }

  return WaitTimeStateEnum.Specified;
};
