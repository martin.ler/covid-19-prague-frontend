import { DataBox } from 'components/dataBox';
import { FiltersBox } from 'components/filtersBox';
import { LanguageSwitch } from 'components/languageSwitch';
import { useTranslation } from 'react-i18next';
import { useAnalytics } from 'hooks/analytics';
import React, { FC, useCallback, useContext, useEffect, useState } from 'react';
import { Filters, MethodFilter, DistrictFilter, PriceFilter } from 'types/filters';
import { SamplingPointsServiceRequestTypeEnum } from 'api';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import './mainView.scss';
import { Faq } from 'components/faq';
import { Link } from 'components/link';
import { OldDataWarning } from 'components/oldDataWarning';
import { DataProvider } from 'state/dataState';
import { LanguageContext } from 'state/languageState';

export const MainView: FC = () => {
  const { t } = useTranslation();
  const [analytics] = useAnalytics();
  const { language } = useContext(LanguageContext);

  const [filters, setFilters] = useState<Filters>({
    districtFilter: DistrictFilter.PHA,
    paymentFilter: SamplingPointsServiceRequestTypeEnum.Application,
    methodFilter: MethodFilter.WALK_WITH_RESERVATION,
    priceFilter: PriceFilter.ANY,
  });

  useEffect(
    () => {
      analytics.changeFilter(filters, true);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const changeFilters = useCallback(
    (newFilters: Filters) => {
      analytics.changeFilter(newFilters, false);
      setFilters(newFilters);
    },
    [setFilters, analytics]
  );

  const [isFaqOpen, setFaqOpen] = useState(false);

  const closeFaq = useCallback(() => {
    setFaqOpen(false);
  }, []);

  const openFaq = useCallback(() => {
    setFaqOpen(true);
  }, []);

  return (
    <DataProvider filters={filters} lang={language}>
      <div className="main-view__container">
        <div className="main-view__header-bezpecnost">
          <Container className="py-4">
            <Row>
              <Col md={8} xs={12}>
                <img
                  src={`${process.env.PUBLIC_URL}/prague_logo.svg`}
                  alt="Domovská stránka Praha"
                  className="main-view__header-logo float-left mr-4"
                />
                <Link href="https://bezpecnost.praha.eu/" target="_blank">
                  <h2>Bezpečnost. Praha.&nbsp;eu</h2>
                </Link>
              </Col>
              <Col
                md={4}
                className="main-view__link-portal text-md-right text-sm-left d-none d-md-block"
              >
                <Link href="https://golemio.cz/" target="_blank">
                  <img
                    src={`${process.env.PUBLIC_URL}/logo_golemio.svg`}
                    alt="Golemio - Pražská datová platforma"
                    className="main-view__header-golemio-logo"
                  />
                </Link>
              </Col>
            </Row>
          </Container>
        </div>
        <div className="main-view__header">
          <Container className="py-4">
            <Row>
              <Col
                md={{ span: 9, order: 'first' }}
                xs={{ span: 12, order: 'last' }}
                className="my-2"
              >
                <h1>{t('title')}</h1>
                <h2>{t('subtitle')}</h2>
              </Col>
              <Col
                md={{ span: 3, order: 'last' }}
                xs={{ span: 12, order: 'first' }}
                className="my-2 text-md-right text-sm-left"
              >
                <LanguageSwitch />
              </Col>
            </Row>
            <FiltersBox filters={filters} changeFilters={changeFilters} />
          </Container>
        </div>
        <div className="main-view__section">
          <Container>
            <DataBox openFaq={openFaq} filters={filters} />
          </Container>
        </div>
        <div className="main-view__footer">
          <Container className="py-4">
            <Row>
              <Col
                md={{ span: 4, order: 'first' }}
                xs={{ span: 12, order: 'last' }}
                className="justify-content-md-start"
              >
                <Navbar.Text className="p-3">Operátor ICT, 2020</Navbar.Text>
              </Col>
              <Col md={8} xs={12}>
                <Nav className="justify-content-md-end text-uppercase">
                  <Nav.Link href="https://golemio.cz" target="_blank" className="p-3">
                    {t('footer.golemio')}
                  </Nav.Link>
                  <Nav.Link
                    href="https://gitlab.com/operator-ict/golemio/code/covid-19-prague/covid-19-prague-frontend"
                    target="_blank"
                    className="p-3"
                  >
                    {t('footer.sourcecode')}
                  </Nav.Link>
                  <Nav.Link href="https://covidapi.docs.apiary.io" target="_blank" className="p-3">
                    {t('footer.apidocumentation')}
                  </Nav.Link>
                  <Nav.Link href="#" onClick={() => setFaqOpen(true)} className="p-3">
                    {t('faq')}
                  </Nav.Link>
                </Nav>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <Faq open={isFaqOpen} onHide={closeFaq}></Faq>
      <OldDataWarning />
    </DataProvider>
  );
};
