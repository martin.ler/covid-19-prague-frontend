import classNames from 'classnames';
import React, { FC, ReactNode } from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import './simpleTooltip.scss';

export const SimpleTooltip: FC<{
  children: ReactNode;
  id: string;
  text: ReactNode | null;
  outdated?: boolean;
  underline?: boolean;
}> = ({ children, id, text, underline, outdated }) => {
  if (text === null) {
    return <span>{children}</span>;
  }

  return (
    <OverlayTrigger
      overlay={
        <Tooltip id={id}>
          <span className="simple-tooltip">{text}</span>
        </Tooltip>
      }
    >
      <span
        className={classNames(
          'simple-tooltip-children',
          {
            'simple-tooltip-children__underline': underline,
          },
          outdated ? 'text-danger' : ''
        )}
      >
        {children}
      </span>
    </OverlayTrigger>
  );
};
