import { useAnalytics } from 'hooks/analytics';
import * as React from 'react';

interface Props extends React.HTMLProps<HTMLAnchorElement> {
  ignoreAnalytics?: boolean;
}

export const Link: React.FC<Props> = ({ ignoreAnalytics, children, onClick, ...rest }) => {
  const [analytics] = useAnalytics();

  const handleClick = (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    if (!ignoreAnalytics && rest.href) {
      analytics.openExternalLink(rest.href);
    }
    onClick?.(event);
  };

  return (
    <a onClick={handleClick} {...rest}>
      {children}
    </a>
  );
};
