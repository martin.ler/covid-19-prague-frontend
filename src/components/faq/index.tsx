import { Link } from 'components/link';
import React, { FC, Fragment } from 'react';
import { Alert, Modal, Row, Col } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import './faq.scss';

export const Faq: FC<{ open: boolean; onHide: () => void }> = ({ open, onHide }) => {
  const { t } = useTranslation();

  return (
    <Modal show={open} onHide={onHide} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <h2 className="text-primary">{t('faqTitle')}</h2>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Alert variant="warning">
          <Alert.Heading>{t('isSomethingWrong')}</Alert.Heading>
          <p>{t('isSomethingWrongDescription')}</p>
          <hr />
          <p>
            {t('emailUs')}
            <Link href="mailto:golemio@operatorict.cz?subject=covidOdberovaMista">
              {window.config.EMAIL}
            </Link>
          </p>
        </Alert>

        {(t('faqContent', { returnObjects: true }) as { title: string; description: string }[]).map(
          ({ title, description }, index) => (
            <Fragment key={index}>
              <h3 className="text-primary">{title}</h3>
              <p dangerouslySetInnerHTML={{ __html: description }} />
            </Fragment>
          )
        )}

        <Row>
          {[
            {
              title: 'Hlavní město Praha',
              src: 'logo_praha.png',
            },
            {
              title: 'ÚZIS ČR',
              src: 'logo_uziscr.png',
            },
            {
              title: 'Armáda ČR',
              src: 'logo_armycr.png',
            },
            {
              title: 'Sator IT',
              src: 'logo_satorit.png',
            },
            {
              title: 'Česko Digital',
              src: 'logo_ceskodigital.png',
            },
            {
              title: 'Keboola',
              src: 'logo_keboola.png',
            },
          ].map((partner, index) => (
            <Col key={index} xs={4} className="p-5 text-center">
              <h4>{partner.title}</h4>
              <img
                src={`${process.env.PUBLIC_URL}/${partner.src}`}
                alt={`Logo ${partner.title}`}
                className="faq__img"
              ></img>
            </Col>
          ))}
        </Row>
      </Modal.Body>
    </Modal>
  );
};
