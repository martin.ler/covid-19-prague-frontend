import React, { FC, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { Filters, MethodFilter, DistrictFilter, PriceFilter } from 'types/filters';
import './filtersBox.scss';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { SamplingPointsServiceRequestTypeEnum } from 'api';
import { FilterButton } from 'components/filterButton';
import { prices } from 'hooks/priceFilter';

export const FiltersBox: FC<{ filters: Filters; changeFilters: (filters: Filters) => void }> = ({
  filters,
  changeFilters,
}) => {
  const { t } = useTranslation();

  const changeFilter = useCallback(
    (name, value) => {
      changeFilters({ ...filters, [name]: value });
    },
    [filters, changeFilters]
  );

  const isPhaDistrictFilterActive = filters.districtFilter === DistrictFilter.PHA;
  const isStcDistrictFilterActive = filters.districtFilter === DistrictFilter.STC;
  const isAnyDistrictFilterActive = filters.districtFilter === DistrictFilter.ANY;

  return (
    <div className="filters-box">
      <div className="filters-box__filters-container">
        <Row className="filters-box__filter">
          <Col lg={3} xs={6}>
            <h3>{t('filters.district')}</h3>
            <FilterButton
              iconName="PhaStc"
              phaStcActive="pha"
              variant="outline-primary"
              isActive={isPhaDistrictFilterActive}
              action={() => changeFilter('districtFilter', DistrictFilter.PHA)}
            >
              {t('filters.prague')}
            </FilterButton>
            <FilterButton
              iconName="PhaStc"
              phaStcActive="stc"
              variant="outline-primary"
              isActive={isStcDistrictFilterActive}
              action={() => changeFilter('districtFilter', DistrictFilter.STC)}
            >
              {t('filters.centralBohemia')}
            </FilterButton>
            <FilterButton
              iconName="PhaStc"
              phaStcActive="both"
              variant="outline-primary"
              action={() => changeFilter('districtFilter', DistrictFilter.ANY)}
              isActive={isAnyDistrictFilterActive}
            >
              {t('filters.any')}
            </FilterButton>
          </Col>
          <Col lg={3} xs={6}>
            <h3>{t('filters.paymentType')}</h3>
            <FilterButton
              iconName={'Request'}
              variant="outline-primary"
              isActive={filters.paymentFilter === SamplingPointsServiceRequestTypeEnum.Application}
              action={() =>
                changeFilter('paymentFilter', SamplingPointsServiceRequestTypeEnum.Application)
              }
            >
              {t('filters.withRequest')}
            </FilterButton>
            <FilterButton
              iconName={'Payment'}
              variant="outline-primary"
              action={() =>
                changeFilter('paymentFilter', SamplingPointsServiceRequestTypeEnum.Selfpaid)
              }
              isActive={filters.paymentFilter === SamplingPointsServiceRequestTypeEnum.Selfpaid}
            >
              {t('filters.selfpaid')}
            </FilterButton>
          </Col>
          <Col lg={3} xs={6}>
            <h3>{t('filters.method')}</h3>

            <FilterButton
              iconName={'WalkReservation'}
              variant="outline-primary"
              isActive={filters.methodFilter === MethodFilter.WALK_WITH_RESERVATION}
              action={() => changeFilter('methodFilter', MethodFilter.WALK_WITH_RESERVATION)}
            >
              {t('filters.walkWithReservation')}
            </FilterButton>
            <FilterButton
              iconName={'Walk'}
              variant="outline-primary"
              isActive={filters.methodFilter === MethodFilter.WALK_WITHOUT_RESERVATION}
              action={() => changeFilter('methodFilter', MethodFilter.WALK_WITHOUT_RESERVATION)}
            >
              {t('filters.walkWithoutReservation')}
            </FilterButton>
            <FilterButton
              iconName={'DriveIn'}
              variant="outline-primary"
              isActive={filters.methodFilter === MethodFilter.IN_CAR}
              action={() => changeFilter('methodFilter', MethodFilter.IN_CAR)}
            >
              {t('filters.inCar')}
            </FilterButton>
          </Col>
          <Col lg={3} xs={6}>
            <h3>{t('filters.price')}</h3>

            {filters.paymentFilter === SamplingPointsServiceRequestTypeEnum.Selfpaid && (
              <>
                <FilterButton
                  iconName={'Coins1'}
                  variant="outline-primary"
                  isActive={filters.priceFilter === PriceFilter.LOW}
                  action={() => changeFilter('priceFilter', PriceFilter.LOW)}
                >
                  {t('filters.priceOrLess', { price: prices.low })}
                </FilterButton>
                <FilterButton
                  iconName={'Coins2'}
                  variant="outline-primary"
                  isActive={filters.priceFilter === PriceFilter.HIGH}
                  action={() => changeFilter('priceFilter', PriceFilter.HIGH)}
                >
                  {t('filters.priceOrLess', { price: prices.high })}
                </FilterButton>
                <FilterButton
                  iconName={'Coins3'}
                  variant="outline-primary"
                  isActive={filters.priceFilter === PriceFilter.ANY}
                  action={() => changeFilter('priceFilter', PriceFilter.ANY)}
                >
                  {t('filters.any')}
                </FilterButton>
              </>
            )}
            {filters.paymentFilter === SamplingPointsServiceRequestTypeEnum.Application && (
              <p className="text-secondary text-center my-5">{t('priceFilterInfo')}</p>
            )}
          </Col>
        </Row>
      </div>
    </div>
  );
};
