import React, { FC } from 'react';
import { Icon } from 'components/icon';
import {
  SamplingPointDistrictEnum,
  SamplingPointsService,
  SamplingPointsServiceProcessTypeEnum,
  SamplingPointsServiceQueueTypeEnum,
  SamplingPointsServiceRequestTypeEnum,
} from 'api';
import { prices, usePriceFilter } from 'hooks/priceFilter';
import { useMethodFilter } from 'hooks/methodFilter';
import { usePaymentFilter } from 'hooks/paymentFilter';
import { Filters, PriceFilter } from 'types/filters';
import './serviceIcons.scss';
import { useTranslation } from 'react-i18next';
import { SimpleTooltip } from 'components/simpleTooltip';
import { useDistrictFilter } from 'hooks/districtFilter';
import { Col, Row } from 'react-bootstrap';

const ICON_SIZE = 20;

export const ServiceIcons: FC<{
  filters: Filters;
  service: SamplingPointsService;
  district?: SamplingPointDistrictEnum;
  colors?: 'inverted' | 'normal';
  isInDetail?: boolean;
}> = ({ filters, service, colors = 'normal', district, isInDetail = false }) => {
  const { t } = useTranslation();

  const { hasAppropriatePrice } = usePriceFilter();
  const { hasAppropriateMethod } = useMethodFilter();
  const { hasAppropriatePayment } = usePaymentFilter();
  const { hasAppropriateDistrict } = useDistrictFilter();

  const paymentColor =
    !filters || hasAppropriatePayment(service, filters) ? 'primary' : 'secondary';
  const methodColor = hasAppropriateMethod(service, filters) ? 'primary' : 'secondary';
  const priceColor = hasAppropriatePrice(service, filters) ? 'primary' : 'secondary';
  const districtColor =
    district && hasAppropriateDistrict(district, filters) ? 'primary' : 'secondary';

  const iconWidth = isInDetail ? 3 : 6;
  const districtTooltipText =
    district === SamplingPointDistrictEnum.PHA
      ? t('filters.prague')
      : district === SamplingPointDistrictEnum.STC
      ? t('filters.centralBohemia')
      : t('filters.pragueAndCentralBohemia');
  const phaStcActiveValue =
    district === SamplingPointDistrictEnum.PHA
      ? 'pha'
      : district === SamplingPointDistrictEnum.STC
      ? 'stc'
      : 'both';

  return (
    <Row className="service-icons__container no-gutters">
      {district && (
        <Col xs={iconWidth}>
          <SimpleTooltip id="" text={districtTooltipText}>
            <Icon
              inverted={colors === 'inverted'}
              iconName="PhaStc"
              size={ICON_SIZE}
              color={districtColor}
              phaStcActive={phaStcActiveValue}
            />
          </SimpleTooltip>
        </Col>
      )}
      {service.requestType === SamplingPointsServiceRequestTypeEnum.Selfpaid && (
        <Col xs={iconWidth}>
          <SimpleTooltip id="" text={t('filters.selfpaid')}>
            <Icon
              inverted={colors === 'inverted'}
              iconName="Payment"
              size={ICON_SIZE}
              color={paymentColor}
            />
          </SimpleTooltip>
        </Col>
      )}
      {service.requestType === SamplingPointsServiceRequestTypeEnum.Application && (
        <Col xs={iconWidth}>
          <SimpleTooltip id="" text={t('filters.withRequest')}>
            <Icon
              inverted={colors === 'inverted'}
              iconName="Request"
              size={ICON_SIZE}
              color={paymentColor}
            />
          </SimpleTooltip>
        </Col>
      )}
      {service.processType === SamplingPointsServiceProcessTypeEnum.Drivein && (
        <Col xs={iconWidth}>
          <SimpleTooltip id="" text={t('filters.inCar')}>
            <Icon
              inverted={colors === 'inverted'}
              iconName="DriveIn"
              size={ICON_SIZE}
              color={methodColor}
            />
          </SimpleTooltip>
        </Col>
      )}
      {service.processType === SamplingPointsServiceProcessTypeEnum.Walk &&
        service.queueType === SamplingPointsServiceQueueTypeEnum.Queue && (
          <Col xs={iconWidth}>
            <SimpleTooltip id="" text={t('filters.walkWithoutReservation')}>
              <Icon
                inverted={colors === 'inverted'}
                iconName="Walk"
                size={ICON_SIZE}
                color={methodColor}
              />
            </SimpleTooltip>
          </Col>
        )}
      {service.processType === SamplingPointsServiceProcessTypeEnum.Walk &&
        service.queueType === SamplingPointsServiceQueueTypeEnum.Reservation && (
          <Col xs={iconWidth}>
            <SimpleTooltip id="" text={t('filters.walkWithReservation')}>
              <Icon
                inverted={colors === 'inverted'}
                iconName="WalkReservation"
                size={ICON_SIZE}
                color={methodColor}
              />
            </SimpleTooltip>
          </Col>
        )}
      {!!service.price && (
        <>
          {(hasAppropriatePrice(service, {
            priceFilter: PriceFilter.LOW,
            paymentFilter: SamplingPointsServiceRequestTypeEnum.Selfpaid,
          }) && (
            <Col xs={iconWidth}>
              <SimpleTooltip id="" text={t('filters.priceOrLess', { price: prices.low })}>
                <Icon
                  inverted={colors === 'inverted'}
                  iconName="Coins1"
                  size={ICON_SIZE}
                  color="primary"
                />
              </SimpleTooltip>
            </Col>
          )) ||
            (hasAppropriatePrice(service, {
              priceFilter: PriceFilter.HIGH,
              paymentFilter: SamplingPointsServiceRequestTypeEnum.Selfpaid,
            }) && (
              <Col xs={iconWidth}>
                <SimpleTooltip id="" text={t('filters.priceOrLess', { price: prices.high })}>
                  <Icon
                    inverted={colors === 'inverted'}
                    iconName="Coins2"
                    size={ICON_SIZE}
                    color={priceColor}
                  />
                </SimpleTooltip>
              </Col>
            )) || (
              <Col xs={iconWidth}>
                <SimpleTooltip id="" text={t('filters.priceMoreThan', { price: prices.high })}>
                  <Icon
                    inverted={colors === 'inverted'}
                    iconName="Coins3"
                    size={ICON_SIZE}
                    color={priceColor}
                  />
                </SimpleTooltip>
              </Col>
            )}
        </>
      )}
      {!service.price && (
        <Col xs={iconWidth}>
          <SimpleTooltip id="" text={t('filters.priceNotMatter')}>
            <Icon
              inverted={colors === 'inverted'}
              iconName="Coins1"
              size={ICON_SIZE}
              color="secondary"
            />
          </SimpleTooltip>
        </Col>
      )}
    </Row>
  );
};
