FROM node:12

WORKDIR /usr/src/app/

RUN yarn global add serve

COPY --chown=node:node ./dist/ .

EXPOSE 3000
CMD ["serve","-s", "-l", "3000"]
