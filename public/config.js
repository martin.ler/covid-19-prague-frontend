// config map - this file gets replaced by env config during deployment
window.config = {
  API_URL: 'https://rabin.golemio.cz/v1/covid-odberova-mista-api',
  MAPBOX_TOKEN:
    'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA',
  EMAIL: 'golemio@operatorict.cz',
  GA_CODE: 'UA-179230069-2',
  // delay in seconds before displaying old data notification
  DATA_REFRESH_WARNING_DELAY: 180,
};
